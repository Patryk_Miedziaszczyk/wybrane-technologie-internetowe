import redis

r = redis.Redis(
    host='localhost',
    port=6381)


def create_queue():
    if r.exists("myQueue"):
        r.delete("myQueue")
    r.lpush("myQueue", "Task 1")


def add_message(message):
    r.lpush("myQueue", str(message))


def delete_last():
    r.ltrim("myQueue", 0, -2)
    queue = r.lrange("myQueue", 0, 10)
    print(queue)


def get_last():
    last = r.lrange("myQueue", -1, -1)
    print(last)
    delete_last()

def delete_queue():
    r.delete("myQueue")

def print_queue():
    if not r.exists("myQueue"):
        print("Queue doesn't exist")
    queue = r.lrange("myQueue", 0, -1)
    print(queue)
print('1. Create queue \n2. Add message \n3. Get last \n4. Delete last \n5. Delete queue \n6. Print queue \n')

while True:
    choice = input("Number of function: ")
    if choice == '1':
        create_queue()
    elif choice == '2':
        message = input("Type message: ")
        add_message(message)
    elif choice == '3':
        get_last()
    elif choice == '4':
        delete_last()
    elif choice == '5':
        delete_queue()
    elif choice == '6':
        print_queue()